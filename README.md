chipBoy8
========
> A CHIP-8 interpreter written in C++ using SDL2.

Even though there are still some bugs left, the code is pretty much functional already by making a good number of games playable. The code has only been tested on Windows (see the following note) and ArchLinux, but Mac OS X should be supported, as well as the different *BSD flavours.

> IMPORTANT NOTE: Originally in 2014 the program was built using CMake and was pretty much fully portable across systems. I've decided to change it to a plain-old Makefile despite that due to a bunch of uninteresting reasons, the main one being that I find it easier to deal with Make than CMake. I've tagged the old version as "1.0" in the extremely rare case anyone's ever interested in building chipBoy8 in Windows (POSIX compliant systems should be safe from this, but I have my doubts with MinGW due to the use of sdl2-config). On the bright side, now it's far easier to understand what's going on during building, you can trust dependency handling and the filenames are not hard-coded into CMakeList.

## Usage
As of now, you can execute a CHIP-8 program by:

  - Dragging and dropping the CHIP-8 binary over the interpreter executable
  - Executing the interpreter in the command line with the CHIP-8 program path as its first argument

## Dependencies and building instructions (v1.0b)

In order to build **chipBoy8**, [SDL2](https://www.libsdl.org/download-2.0.php) and **GNU Make** are needed. **SDL2**'s development libraries for compilation and its runtime libraries for execution of the binary.

From v1.0 onwards (v1.0b stand for "v1.0 branched") plain-old Makefiles are used in order to build (see the note above).

In the simplest case, this should compile quietly (no ./configure is required)

1. `cd <repository root>`
2. `make`
3. `./chipBoy8 <path to a ROM>`

If you are half as lazy as I am, you'd rather have a shortcut to open games. Well, just edit the Makefile and add the path to the rom under the target "run", then by issuing `make run` you get an incremental build that starts up the emulator as soon as the compilation is done. With `make clean` you can get rid of temporary build files, and with `make clean run` you get a non-incremental compilation.


## Dependencies and building instructions (<=v1.0)

In order to build **chipBoy8**, [SDL2](https://www.libsdl.org/download-2.0.php) and [CMake](http://www.cmake.org/download/) are needed. **SDL2**'s development libraries for compilation and its runtime libraries for execution of the binary.

Up until v1.0 this project used **CMake** to ensure portability across different platforms and/or compilers.

> NOTE FOR WINDOWS: As Windows lacks default *include* or *lib* folders, the development libraries must be extracted somewhere else. It doesn't really matter, but an environment variable named **SDL2** must be created showing their path (e.g: `$(SDL2)=C:\SDL`), so that **CMake** knows where to look for them.

  1. `cd <repository root>`
  2. `mkdir build`
  3. `cd build`
  4. `cmake ..`

The **build** directory should be populated now with whatever files your compiler needs. In case of Visual Studio on Windows those are *.sln* and *.vcxproj* files (among others). CMake can be instructed to populate **build** with files for other compilers (Projects for XCode or Makefiles, for instance), as explained in [the documentation of CMake](http://www.cmake.org/cmake/help/v3.0/manual/cmake.1.html)

The project must be compiled now. In Visual Studio that is pressing *F5* in the IDE, while for *NIX systems most likely `./configure && make` is the way to go.

## TODO
  - Fix bugs (known and unknown)
  - Check if timing is consistent across different machines
  - Make use of SDL's sound API to emulate the buzzer
  - Remake the gigantic opcode-decoding switch as function pointer arrays
  - Introduce wxWidgets in order to load games easily