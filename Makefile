CXX  	 := g++
CPPFLAGS := -MMD -MP
CXXFLAGS := $(shell sdl2-config --cflags) -g -O3 -std=c++11 -Wall -Wextra -pedantic
LIBS	 := $(shell sdl2-config --libs)

SRCFILES := $(shell find . -type f -name "*.cpp")
OBJFILES := $(SRCFILES:.cpp=.o)
DEPFILES := $(SRCFILES:.cpp=.d)

chipBoy8: $(OBJFILES)
	@$(CXX) -o $@ $(OBJFILES) $(LIBS)

run: chipBoy8
	@./chipBoy8

clean:
	@rm -rf $(OBJFILES) $(DEPFILES) chipBoy8

%.o: %.cpp
	@$(CXX) $(CXXFLAGS) $(CPPFLAGS) -c -o $@ $<

-include $(DEPFILES)
